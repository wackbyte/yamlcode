"use strict";
/* eslint-disable no-console, no-unused-vars */

const YAML = require("yaml");

const env = {
	exec: (_, a) => env[a](),
	log: (_, a) => console.log(a),
	add: (_, a, b) => a + b,
	sub: (_, a, b) => a - b,
	mul: (_, a, b) => a * b,
	div: (_, a, b) => a / b,
	mod: (_, a, b) => a % b,
	set: (_, a, b) => (env[a] = b),
	get: (_, a) => env[a],
	neq: (_, a, b) => a !== b,
	eq: (_, a, b) => a === b,
	lneq: (_, a, b) => a != b,
	leq: (_, a, b) => a == b,
	not: (_, a) => !a,
	and: (_, a, b) => a && b,
	or: (_, a, b) => a || b,
};

function evaluate(expr) {
	// It's not an object, so let's just spit it back out.
	if (
		typeof expr === "number" ||
		typeof expr === "string" ||
		typeof expr === "boolean"
	)
		return expr;

	const command = Object.keys(expr)[0];
	let args = expr[command];
	// It has arguments, so it must be a function. Let's run it!
	if (args) {
		if (!(args instanceof Array)) args = [args];
		args = args.map(n => {
			if (n instanceof Object && !(n instanceof Array)) {
				if (Object.keys(n).length === 1) return evaluate(n);
				return n;
			}
			return n;
		});
		const out = env[command](expr, ...args);
		return out;
	}
	// It doesn't have arguments, must be a keyword.
	switch (null) {
		case expr.func:
			env[expr.name] = () => {
				expr.commands.forEach(evaluate);
			};
			break;
		case expr.repeat: {
			let times = expr.times;
			// Typecast times as a number in case it isn't already
			if (typeof times === "string") times = Number(env[times]);
			// If they've specified a variable to bind it to.
			if (expr.var) {
				for (env[expr.var] = 0; env[expr.var] < times; env[expr.var]++)
					expr.commands.forEach(evaluate);
			} else {
				// If not, we can just use let.
				for (let i = 0; i < times; i++) expr.commands.forEach(evaluate);
			}
			break;
		}
		case expr.if: {
			let condition = expr.condition;
			condition = env[condition];
			if (condition) {
				expr.then.forEach(evaluate);
			} else if (expr.else) {
				expr.else.forEach(evaluate);
			}
			break;
		}
		case expr.while:
			while (env[expr.condition]) expr.commands.forEach(evaluate);
			break;
		default:
			throw new Error(
				`It looks like you tried to call a function but it doesn't exist.
==========
At function ${Object.keys(expr)[0]}`
			);
	}
	return false;
}

module.exports = function parse(text) {
	let yaml;
	try {
		yaml = YAML.parse(text);
	} catch (e) {
		throw new Error("Your YAML is invalid!");
	}
	yaml.forEach(expr => evaluate(expr));
};
